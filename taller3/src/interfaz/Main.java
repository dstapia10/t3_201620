package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
	  System.out.println("** REGISTRAR CLIENTE **");
	  System.out.println("Ingrese nombre de Conductor");
	  String nombreConductor = br.readLine();
	  System.out.println("Ingrese color de carro");
	  String colorCarro = br.readLine();
	  System.out.println("Ingrese matricula de carro");
	  String matriculaCarro = br.readLine();
	  central.registrarCliente(colorCarro, matriculaCarro, nombreConductor);
	  System.out.println("Se a registrado el nuevo cliente");
	  System.out.println("");
  }
	
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  System.out.println("** PARQUEAR CARRO EN COLA **");	  
	  System.out.println("");
	  System.out.println(""+central.parquearCarroEnCola());
	  System.out.println("");
  }
  
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("** REGISTRAR SALIDA CLIENTE **");
	  System.out.println("");
	  System.out.println("Ingrese matricula del carro");
	  String matriculaCarro = br.readLine();	  
	  System.out.println("Tarifa: $"+central.atenderClienteSale(matriculaCarro));
	  System.out.println("");
  }
  
  /**
   * M�todo que permite visualizar graficaente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  System.out.println("** ESTADO PARQUEADEROS **");
	  System.out.println("");	 
	  for(int i=0 ; i<8 ; i++)
	  {	 
		  String imprimir = "p"+(i+1)+" ("+central.pilasCarros[i].size()+")";
		  Iterator<Carro> j = central.pilasCarros[i].iterator(); 
		  while (j.hasNext())
		  {
		   	imprimir += " - " + j.next().darMatricula();
		  }		  
		  System.out.println(imprimir);
	  }
	  System.out.println("");	  
  }
  
  
  /**
   * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("** ESTADO COLA **");
	  System.out.println("");
	  String resp = "Carros en espera ("+central.carrosEspera.size()+")";
	  Iterator<Carro> i = central.carrosEspera.iterator(); 
	  while(i.hasNext())
	  {
		  resp += " - "+i.next().darMatricula();
	  }
	  System.out.println(resp);
	  System.out.println("");
  }
  
  	/**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
