package parqueadero;

import java.util.Date;
import java.util.Iterator;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	public LinkedQueue<Carro> carrosEspera;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	public LinkedStack<Carro>[] pilasCarros;
    
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	public LinkedStack<Carro> pilaTemporal;
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
    	pilasCarros = new LinkedStack[8];
    	for(int i=0 ; i<8 ; i++)
    	{
    		pilasCarros[i] = new LinkedStack<Carro>();
    	}
    	
    	pilaTemporal = new LinkedStack<Carro>();
    	
    	carrosEspera = new LinkedQueue<Carro>();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro nuevo = new Carro(pColor, pMatricula, pNombreConductor);
    	carrosEspera.enqueue(nuevo);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicación
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	Carro aMover = carrosEspera.dequeue();
    	return "Matricula: "+aMover.darMatricula()+", "+parquearCarro(aMover);
    } 
    
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	Carro sacar = sacarCarro(matricula);
    	return cobrarTarifa(sacar);
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que quedó el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aCarro) throws Exception
    {
    	String resp="";
    	for(int i =0 ; i<8 && resp=="" ; i++)
    	{
    		if(pilasCarros[i].size()<4)
    		{
    			resp="Parqueadero: "+(i+1); 
    			pilasCarros[i].push(aCarro);    			       			
    		}
    	} 	
    	return resp;    	    
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	Carro resp = null;
    	/**
    	for(int i =0 ; i<8 && resp==null ; i++)
    	{
    		int tamañoPila=pilasCarros[i].size();
    		for(int j =0 ; j<tamañoPila && resp==null ; j++)
        	{
    			Carro probando = pilasCarros[i].pop();    			
    			if(probando.darMatricula().equals(matricula))resp=probando;
    			else pilaTemporal.push(probando);
        	}
    		int tamañoPilarTemp = pilaTemporal.size();
    		for(int x =0 ; x<tamañoPilarTemp && resp==null ; x++)
        	{
    			Carro probandoTemporal = pilaTemporal.pop();    			
    			pilasCarros[i].push(probandoTemporal);
        	}
    	}
    	*/
    	
    	for(int i =0 ; i<8 && resp==null ; i++)
    	{
			Iterator<Carro> j = pilasCarros[i].iterator(); 
			while (j.hasNext() && resp==null)
			{
				if(j.next().darMatricula().equals(matricula) && resp==null)
				{
					if(i==0)resp=sacarCarroP1(matricula);
					else if(i==1)resp=sacarCarroP2(matricula);
					else if(i==2)resp=sacarCarroP3(matricula);
					else if(i==3)resp=sacarCarroP4(matricula);
					else if(i==4)resp=sacarCarroP5(matricula);
					else if(i==5)resp=sacarCarroP6(matricula);
					else if(i==6)resp=sacarCarroP7(matricula);
					else if(i==7)resp=sacarCarroP8(matricula);
				}
			}		  
    	}    	
    	return resp; 
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[0].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[0].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[0].push(probandoTemporal);
    	}
		return resp;    	
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[1].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[1].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[1].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[2].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[2].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[2].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[3].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[3].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[3].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[4].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[4].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[4].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[5].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[5].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[5].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[6].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[6].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[6].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	Carro resp = null;
    	int tamañoPila=pilasCarros[7].size();	
		for(int i =0 ; i<tamañoPila && resp==null ; i++)
    	{	
			Carro probando = pilasCarros[7].pop();    	
			if(probando.darMatricula().equals(matricula))resp=probando;
			else pilaTemporal.push(probando);
    	}
		int tamañoPilarTemp = pilaTemporal.size();
		for(int x =0 ; x<tamañoPilarTemp; x++)
    	{
			Carro probandoTemporal = pilaTemporal.pop();    			
			pilasCarros[7].push(probandoTemporal);
    	}
		return resp;  
    }
    
    /**
     * Calcula el valor que debe ser cobrado al cliente en función del tiempo que duró un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	Date horaActual = new Date ();
    	long horaActualMillis = horaActual.getTime();
    	long horaLLegaraCarro = car.darLlegada();
    	return ((horaActualMillis-horaLLegaraCarro)/60000)*25;	
    }
}
