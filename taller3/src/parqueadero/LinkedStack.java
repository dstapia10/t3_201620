package parqueadero;

import java.util.Iterator;

/**
 * Informaci�n sobre estructuras sacada del libro Algorithms  de ROBERT SEDGEWICK y KEVIN WAYNE
 */

public class LinkedStack <T> implements Iterable<T> 
{
	
	/**
     * Primer nodo
     */
	private NodeLinkedStack first = null;
	
	/**
     * Tama�o de linked stack
     */
	private int size = 0;	
	
	/**
     * Clase de nodos utilizados en linked stack
     */
	public class NodeLinkedStack 
	{
		T item;
		NodeLinkedStack next;
	}
	
	/**
     * Clase de iterador del linked stack
     */
	public class LinkedStackIterator implements Iterator<T>
	{		
		private NodeLinkedStack current = first;
		public boolean hasNext() 
		{ 
			return current != null; 
		}
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		} 
	} 	
	
	/**
     * Indica si linked stack esta desocupado
     * @return true si esta desocupado, false de lo contrario
     */
	public boolean isEmpty()
	{
		return first==null;
	}
	
	/**
     * Agrega un nuevo elemento al linked stack
     * @param nuevo nuevo elemento a agregar
     */
	public void push(T nuevo)
	{
		NodeLinkedStack viejoPrimero = first;
		first = new NodeLinkedStack();
		first.item = nuevo;
		first.next = viejoPrimero;
		size++;
	}
	
	/**
     * Elimina el primer elemneto del linked stack
     * @return elemento que se elimina
     */
	public T pop()
	{
		T resp = first.item;
		first = first.next;
		size--;
		return resp;
	}
	
	/**
     * Indica el tama�o del linked stack
     * @return tama�o del linked stack
     */
	public int size()
	{
		return size;		
	}
	
	/**
     * Genera un iterador del linked stack
     * @return iterador del linked stack
     */
	public Iterator<T> iterator() 
	{ 
		return new LinkedStackIterator(); 
	}	

}
