package parqueadero;

import java.util.Iterator;

/**
 * Informaci�n sobre estructuras sacada del libro Algorithms  de ROBERT SEDGEWICK y KEVIN WAYNE
 */

public class LinkedQueue<T> implements Iterable<T> 
{	
	
	/**
     * Primer nodo
     */
	private NodeLinkedQueue first;
	
	/**
     * Ultimo nodo
     */
	private NodeLinkedQueue last;
	
	/**
     * Tama�o de linked queue
     */
	private int size = 0;
	
	/**
     * Clase de nodos utilizados en linked queue
     */
	public class NodeLinkedQueue 
	{
		T item;
		NodeLinkedQueue next;
	}
	
	/**
     * Clase de iterador del linked queue
     */
	public class LinkedQueueIterator implements Iterator<T>
	{		
		private NodeLinkedQueue current = first;
		public boolean hasNext() 
		{ 
			return current != null; 
		}
		public T next()
		{
			T item = current.item;
			current = current.next;
			return item;
		} 
	}	
	
	/**
     * Indica si linked queue esta desocupado
     * @return true si esta desocupado, false de lo contrario
     */
	public boolean isEmpty()
	{ 
		return first == null; 
	}
	
	/**
     * Agrega un nuevo elemento al linked queue
     * @param nuevo nuevo elemento a agregar
     */
	public void enqueue(T nuevo)
	{
		NodeLinkedQueue viejoUltimo = last;
		last = new NodeLinkedQueue();
		last.item = nuevo;
		last.next = null;
		if (isEmpty()) first = last;
		else viejoUltimo.next = last;
		size++;
	}
	
	/**
     * Elimina el ultimo elemneto del linked queue
     * @return elemento que se elimina
     */
	public T dequeue()
	{
		T resp = first.item;
		first = first.next;
		if (isEmpty()) last = null;
		size--;
		return resp;
	}
	
	/**
     * Indica el tama�o del linked queue
     * @return tama�o del linked queue
     */
	public int size()
	{
		return size;
	}
	
	/**
     * Genera un iterador del linked queue
     * @return iterador del linked queue
     */
	public Iterator<T> iterator() 
	{ 
		return new LinkedQueueIterator(); 
	}		
	 
}
